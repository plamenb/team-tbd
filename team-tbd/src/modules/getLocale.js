import { initialLoad } from './initialLoad.js';
import { chartUpdater } from './updatingChart.js';

const getLocale = () => {
  if (localStorage.getItem('favCity') !== null) {
    initialLoad.renderByName(localStorage.getItem('favCity'));
    chartUpdater.chartUpdateByName(localStorage.getItem('favCity'));
    setTimeout(() => { 
    $('.far').addClass('hidden');
    $('.fas').removeClass('hidden');
    $('.charts').removeClass('hidden');
  }, 300);
    
  } else {
    const successCallback = (position) => {
      const x = position.coords.latitude;
      const y = position.coords.longitude;
      initialLoad.renderByCoord(x, y);
      $(document).ready(() => {
      chartUpdater.chartUpdateByCoord(x, y);
      $('.charts').removeClass('hidden');
      });
    };

    const failCallBack = () => {
      if (localStorage.getItem('favCity') !== null) {
        initialLoad.renderByName(localStorage.getItem('favCity'));
        chartUpdater.chartUpdateByName(localStorage.getItem('favCity'));
        $('.far').addClass('hidden');
        $('.fas').removeClass('hidden');
        $('.charts').removeClass('hidden');
      } else {
        initialLoad.renderByName('Sofia');
        chartUpdater.chartUpdateByName('Sofia');
        $('.charts').removeClass('hidden');
      }
    };
    navigator.geolocation.getCurrentPosition(successCallback, failCallBack);
  }
};


export const locale = {
  getLocale,
};