import { updateWeatherData } from './updateWeatherData.js';
import { chartUpdater } from './updatingChart.js'

const cityClick = (cityName) => {
  if (cityName !== localStorage.getItem('favCity')) {
    $('.far').removeClass('hidden');
    $('.fas').addClass('hidden');
    updateWeatherData.render(cityName);
    chartUpdater.chartUpdateByName(cityName)
  } else {
    $('.far').addClass('hidden');
    $('.fas').removeClass('hidden');
    updateWeatherData.render(cityName);
    chartUpdater.chartUpdateByName(cityName)
  }
};

export const click = {
  cityClick,
};