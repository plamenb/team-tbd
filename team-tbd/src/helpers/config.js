export const config = {
  selectors: {
    city: '.cityCountry',
    temperature: '#temperature',
    pressure: '#pressure',
    day: '.container',
    dailyTemperature: '#daily-temperature',
    containerDaily: '.container-daily',
    iconUrl: './src/assets/icons/',
    degrees: '.degree',
    weather: '.currWeather',
    humidity: '.humidity',
    wind: '.wind',
    barometer: '.barometer',
    sunrise: '.sunrise',
    sunset: '.sunset',
    mainIcon: '#main-icon',
  },
  keys: {
    weatherApiKey: 'bd6d48a8fbb8878d55ceec9ae0cb903f',
    dailyWeatherApiKey: '8ce611c24062445a8103d62f187d076e' //'9591750b572f4a06ad2c58eb7e83e00e' // '9591750b572f4a06ad2c58eb7e83e00e' // '8ce611c24062445a8103d62f187d076e', // '67a3ecbb662c4060b33cbd6bb6201f1b',
  },
};
