import { config } from "../helpers/config.js";
import { database } from "../helpers/database.js";

const displayDailyInfo = (data) => {
  for (let i = 0; i < 5; i++) {
    let currentContainer = $(config.selectors.day)[i];
    currentContainer.children[1].src = config.selectors.iconUrl + data.data[i].weather.icon + '.svg';
    currentContainer.children[2].innerHTML = `${Math.round(data.data[i].max_temp)}° \u00A0\u00A0 ${Math.round(data.data[i].min_temp)}°`;
    currentContainer.children[3].innerHTML = data.data[i].weather.description;
    };
};

const cityChange = (data) => {
  $(config.selectors.city).html(data.data[0].city_name + ', ' + data.data[0].country_code);
  $(config.selectors.degrees).html(Math.round(data.data[0].temp) + '°');
  $(config.selectors.weather).html(data.data[0].weather.description);
  $(config.selectors.humidity).html(`Humidity ${data.data[0].rh} %`);
  $(config.selectors.wind).html(`Wind ${Math.round(data.data[0].wind_spd)} kmph`);
  $(config.selectors.barometer).html(`Barometer ${Math.round(data.data[0].pres)} in`);
  $(config.selectors.sunrise).html(`Sunrise: ${data.data[0].sunrise} am`);
  $(config.selectors.sunset).html(`Sunset: ${data.data[0].sunset} pm`);
  $(config.selectors.mainIcon).attr('src', `${config.selectors.iconUrl}${data.data[0].weather.icon}.svg`);
};

const render = (cityName) => {
    database.getDailyWeatherData(cityName)
        .then(displayDailyInfo);
    database.getWeatherByName(cityName)
        .then(cityChange);
};

export const updateWeatherData = {
    render,
};