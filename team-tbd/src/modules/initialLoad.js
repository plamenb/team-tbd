import { config } from "../helpers/config.js";
import { database } from "../helpers/database.js";

const cityChange = (data) => {
    $('.degrees').prepend(`<img id="main-icon" src=${config.selectors.iconUrl}${data.data[0].weather.icon}.svg>`);
    $('.cf').append(`<p class="celsius">${'C'}</p>`);
    $('.cf').append(`<p class="fahrenheit">${'F'}</p>`);
    $('body').append(`<img class="hammer" src="https://img.icons8.com/color/50/000000/thor-hammer.png"><div class="team">Team Thunder</div>`);
    $('.nameFav').append('<i class="far fa-star"></i>');
    $('.nameFav').append('<i class="fas fa-star hidden"></i>');
    $(config.selectors.city).html(data.data[0].city_name + ', ' + data.data[0].country_code);
    $(config.selectors.degrees).html(Math.round(data.data[0].temp) + '°');
    $(config.selectors.weather).html(data.data[0].weather.description);
    $(config.selectors.humidity).html(`Humidity ${data.data[0].rh} %`);
    $(config.selectors.wind).html(`Wind ${Math.round(data.data[0].wind_spd)} kmph`);
    $(config.selectors.barometer).html(`Barometer ${Math.round(data.data[0].pres)} in`);
    $(config.selectors.sunrise).html(`Sunrise: ${data.data[0].sunrise} am`);
    $(config.selectors.sunset).html(`Sunset: ${data.data[0].sunset} pm`);
};

const displayDailyInfo = (data) => {
    for (let i = 0; i < 5; i++) {
        $('.parentContainer').append(`<div class="container ${i}"></div>`);
        $(`.${i}`).append(`<div class="day">${(moment().add(i, 'd').format('ddd'))} \ ${(moment().add(i, 'd').format('D'))}</div>`);
        $(`.${i}`).append(`<img class="icon" src=${config.selectors.iconUrl}${data.data[i].weather.icon}.svg>`);
        $(`.${i}`).append(`<div class="dayHigh">${Math.round(data.data[i].max_temp)}° \u00A0\u00A0 ${Math.round(data.data[i].min_temp)}°</div>`);
        $(`.${i}`).append(`<div class="boxWeather">${data.data[i].weather.description}</div>`);
    };
};



const renderByName = (cityName) => {
    database.getDailyWeatherData(cityName)
        .then(displayDailyInfo);
    database.getWeatherByName(cityName)
        .then(cityChange);
};

const renderByCoord = (coordinateX, coordinateY) => {
    database.getWeatherByCoordinates(coordinateX, coordinateY)
    .then(cityChange);
    database.getDailyWeatherByCoordinates(coordinateX, coordinateY)
    .then(displayDailyInfo);
};

export const initialLoad = {
    renderByName,
    renderByCoord
};