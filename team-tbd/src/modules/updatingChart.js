import {database} from '../helpers/database.js'
import { chart } from './chart.js'

function updateChartTemp(dat) {
    chart.data.datasets[0].data = dat.data.map(hour => hour.temp.toFixed());
    chart.update();
  };
  
function updateChartHours(data) {
  chart.data.labels = data.data.map(hour => hour.timestamp_local.slice(11,16))
    .filter((el, i) => i<8);
      //mapping all local times and filtering only the first 8
  chart.update();
};
  
const chartUpdateByName = (cityName) => {
  database.getHourlyWeather(cityName)
    .then(updateChartTemp);
  database.getHourlyWeather(cityName)
    .then(updateChartHours);
};

const chartUpdateByCoord = (x, y) => {
  database.getHourlyWeatherByCoord(x, y)
    .then(updateChartTemp);
  database.getHourlyWeatherByCoord(x, y)
    .then(updateChartHours);
};
    
function resetChartHours() {
  chart.data.labels = ['00:00', '03:00', '06:00', '09:00', '12:00', '15:00', '18:00', '21:00']
  chart.update();
};
  
const updateChartDay = (event) => {
    database.getHourlyWeather(($('.cityCountry')).text())
      .then(function nextDaysTemp(data) {
        chart.data.datasets[0].data = data.data.map(hour => hour.temp.toFixed())
        .filter((el, i) => i >= ((event.target.className[event.target.className.length - 1])*8)-(8-((24 - Number(data.data[0].timestamp_local.slice(11,13)))/3)));
        chart.update();   
      })
    database.getHourlyWeather(($('.cityCountry')).text())
      .then(resetChartHours)
  };
  
const todayDay = () => {
  $('#0').click(() => {
    chartUpdate(($('.cityCountry')).text());
  });
};
  
export const chartUpdater = {
  chartUpdateByName,
  todayDay,
  updateChartDay,
  chartUpdateByCoord
};
  
  
  