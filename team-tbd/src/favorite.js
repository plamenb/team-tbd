const fav = () => {
  if (!$('.far').hasClass('hidden')) {
    if (localStorage.length === 1) {
      localStorage.removeItem('favCity');
      localStorage.setItem('favCity', $('.cityCountry').html().split(',')[0]);
    } else {
      localStorage.setItem('favCity', $('.cityCountry').html().split(',')[0]);
    }
    $('.far').addClass('hidden');
    $('.fas').removeClass('hidden');
  } else {
    localStorage.removeItem('favCity');
    $('.far').removeClass('hidden');
    $('.fas').addClass('hidden');
  }
};

export const favorite = {
  fav,
};