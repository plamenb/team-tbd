Chart.helpers.merge(Chart.defaults.global.plugins.datalabels, {
  color: '#ffffff',
  align: 'top',
  clip: false
});

const ctx = document.getElementById('myChart').getContext('2d');

const chart = new Chart(ctx, {

  type: 'line',
  data: {
    labels: ['00:00', '03:00', '06:00', '09:00', '12:00', '15:00', '18:00', '21:00'],
    datasets: [{
      lineTension: 0.2,
      pointRadius: 0,
      borderColor: 'rgb(255, 255, 255)',
      data: [8, 8, 8, 8, 8, 8, 8, 8 ],            
        }]
    },
    
  options: {
    layout: {
      padding: {
        left: 50,
        right: 50,
        top: 50,
        bottom: 50
      }
    },
    scales:{
      xAxes: [{
        ticks: {
          fontSize: 12.5,
          fontColor: 'rgb(255, 255, 255)'
        },
        gridLines: {
          display: false
        },
      }],
      yAxes: [{
        ticks: {
          suggestedMin: 0,
          display: false,
          fontColor: 'rgb(255, 255, 255)'
            },
          gridLines: {
            display: false      
            }
        }]
      },
    legend: {
      display: false,
        },
    elements: {
      line: {
        borderWidth: 0.5
        }
      }   
    } 
});

export {
  chart,
};