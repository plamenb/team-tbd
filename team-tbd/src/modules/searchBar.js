import { updateWeatherData } from "./updateWeatherData.js";
import { chartUpdater } from './updatingChart.js'

const search = () => {
  const searchString = $('#search').val();
  if (searchString !== localStorage.getItem('favCity')) {
    $('.far').removeClass('hidden');
    $('.fas').addClass('hidden');
    updateWeatherData.render(searchString);
    chartUpdater.chartUpdateByName(searchString);
  } else {
    $('.far').addClass('hidden');
    $('.fas').removeClass('hidden');
    updateWeatherData.render(searchString);
    chartUpdater.chartUpdateByName(searchString);
  }
  $('#search').val('');
}

export const searchBar = {
  search,
};