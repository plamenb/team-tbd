import { favorite } from './favorite.js';
import { locale } from './modules/getLocale.js';
import { click } from './modules/fiveCities.js';
import { chartUpdater } from './modules/updatingChart.js';
import { searchBar } from './modules/searchBar.js';

let flag = false;
$(document).ready(() => {
  locale.getLocale();
});

$('.nameFav').on('click', '.fa-star', () => {
  favorite.fav();
});

$('.parentContainer').on('click', '.container', (event) => {
  chartUpdater.updateChartDay(event);
});

$('.cities').on('click', '.city', (event) => {
    click.cityClick(event.target.innerHTML);
});

$('#search').keyup((event) => {
  if (event.keyCode === 13) {
    $('.button').click();
  }
});

$('.button').on('click', () => {
  searchBar.search();
});

$('.cf').on('click', '.celsius', () => {
  if (flag === true) {
    $('.degree').html(Math.round(($('.degree').html().split('°')[0] - 32) / 1.8) + '°');
    flag = false;
  }
})

$('.cf').on('click', '.fahrenheit', () => {
  if (flag === false) {
    $('.degree').html(Math.round($('.degree').html().split('°')[0] * 9 / 5 + 32) + '°');
    flag = true;
  };
});

