import { config } from './config.js';

const baseUrl = 'https://api.weatherbit.io/v2.0/current?';
const otherUrl = 'https://api.weatherbit.io/v2.0/forecast/';
const getWeatherByName = (cityName) => {
  return $.get(`${baseUrl}&city=${cityName}&key=${config.keys.dailyWeatherApiKey}`);
};

const getWeatherByCoordinates = (x, y) => {
  return $.get(`${baseUrl}&lat=${x}&lon=${y}&key=${config.keys.dailyWeatherApiKey}`);
};

const getDailyWeatherByCoordinates = (x, y) => {
  return $.get(`${otherUrl}daily?lat=${x}&lon=${y}&key=${config.keys.dailyWeatherApiKey}`);
};

const getDailyWeatherData = (cityName) => {
  return $.get(`${otherUrl}daily?city=${cityName}&days=5&key=${config.keys.dailyWeatherApiKey}`);
};

const getHourlyWeather = (cityName) =>{
  return $.get(`${otherUrl}3hourly?city=${cityName}&key=${config.keys.dailyWeatherApiKey}`);  
};

const getHourlyWeatherByCoord = (x, y) =>{
  return $.get(`${otherUrl}3hourly?lat=${x}&lon=${y}&key=${config.keys.dailyWeatherApiKey}`);  
};

export const database = {
  getWeatherByCoordinates,
  getDailyWeatherByCoordinates,
  getDailyWeatherData,
  getWeatherByName,
  getHourlyWeather,
  getHourlyWeatherByCoord
};
